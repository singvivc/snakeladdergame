BINARY=snakeandladder
test:
	go test -v -cover -covermode=atomic ./...
engine:
	go build -o bin/${BINARY} main.go
unittest:
	go test -short ./...
clean:
	if [ -f bin/${BINARY} ] ; then rm bin/${BINARY} ; fi
cover:
	go test -coverprofile cover.out
view:
	go tool cover -html=cover.out