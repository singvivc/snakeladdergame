package board

// Board struct represent the board on which the game will be played
type board struct {
	cell []int
}

// Board interface
type Board interface {
	NewPosition(int) int
	IsPlayerAtTarget(int) bool
}

// NewBoard function
func NewBoard(cells int, snakes [][]int, ladders [][]int) Board {
	cell := make([]int, cells)
	for _, sp := range snakes {
		cell[sp[0]-1] = sp[1] - 1
	}
	for _, lp := range ladders {
		cell[lp[0]-1] = lp[1] - 1
	}
	return &board{cell: cell}
}

func (b *board) NewPosition(position int) int {
	return validateAndFixPosition(position, b)
}

func (b *board) IsPlayerAtTarget(playerPiecePosition int) bool {
	return playerPiecePosition == len(b.cell)
}

func validateAndFixPosition(position int, b *board) int {
	if position > len(b.cell) {
		return -1
	}
	if position == len(b.cell) {
		return len(b.cell)
	}
	if b.cell[position] == 0 {
		return position
	}
	return b.cell[position]
}
