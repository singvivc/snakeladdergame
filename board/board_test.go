package board

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

type boardStub struct {
	cell   int
	snakes [][]int
	ladder [][]int
}

var tests = []struct {
	description string
	given       boardStub
}{
	{
		"The board with 100 cells",
		boardStub{cell: 100, snakes: [][]int{{45, 23}, {50, 16}}, ladder: [][]int{{34, 64}, {20, 60}}},
	},
	{
		"The board with 10 cells",
		boardStub{cell: 10, snakes: [][]int{{5, 3}, {4, 1}}, ladder: [][]int{{2, 6}, {6, 9}}},
	},
}

func TestShouldInvalidateNewPositionForOutOfRange(t *testing.T) {
	for _, test := range tests {
		board := NewBoard(test.given.cell, test.given.snakes, test.given.ladder)
		position := board.NewPosition(101)
		assert.Equal(t, position, -1)
	}
}

func TestShouldReturnNewPositionThatLiesAtTheEnd(t *testing.T) {
	for _, test := range tests {
		board := NewBoard(test.given.cell, test.given.snakes, test.given.ladder)
		position := board.NewPosition(test.given.cell)
		assert.Equal(t, position, test.given.cell)
	}
}

func TestShouldReturnNewPositionPostBittenBySnake(t *testing.T) {
	for _, test := range tests {
		board := NewBoard(test.given.cell, test.given.snakes, test.given.ladder)
		position := board.NewPosition(test.given.snakes[0][0] - 1)
		assert.Equal(t, position, test.given.snakes[0][1]-1)
	}
}

func TestShouldReturnNewPositionPostEncounteringALadder(t *testing.T) {
	for _, test := range tests {
		board := NewBoard(test.given.cell, test.given.snakes, test.given.ladder)
		position := board.NewPosition(test.given.ladder[0][0] - 1)
		assert.Equal(t, position, test.given.ladder[0][1]-1)
	}
}
