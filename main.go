package main

import (
	"SnakeLadderGame/board"
	"SnakeLadderGame/dice"
	"SnakeLadderGame/game"
	"SnakeLadderGame/player"
)

func main() {
	players := []player.Player{player.NewPlayer("Player A"), player.NewPlayer("Player B")}
	dice := dice.NewDice(1)
	board := board.NewBoard(100, [][]int{{45, 23}, {50, 16}}, [][]int{{34, 64}, {20, 60}})

	game := game.NewGame(board, dice, players)
	game.Start()
}
