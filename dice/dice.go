package dice

import (
	"math/rand"
	"time"
)

type dice struct {
	min int
	max int
}

// Dice interface
type Dice interface {
	Roll() int
}

// NewDice function
func NewDice(numberOfDice int) Dice {
	min, max := 1*numberOfDice, 6*numberOfDice
	return &dice{min, max}
}

func (d *dice) Roll() int {
	rand.Seed(time.Now().UnixNano())
	return randomInt(d.min, d.max)
}

func randomInt(min, max int) int {
	return min + rand.Intn(max-min+1)
}
