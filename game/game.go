package game

import (
	"SnakeLadderGame/board"
	"SnakeLadderGame/dice"
	"SnakeLadderGame/player"
	"fmt"
)

type game struct {
	board   board.Board
	dice    dice.Dice
	players []player.Player
}

// Game interface
type Game interface {
	Start()
}

// NewGame function
func NewGame(board board.Board, dice dice.Dice, players []player.Player) Game {
	return &game{board: board, dice: dice, players: players}
}

func (g *game) Start() {
	var hasWinner bool = false
	var turn int = 0
	var player player.Player
	for !hasWinner {
		player = choosePlayer(g.players, &turn)
		sideValue := player.Roll(g.dice)
		message := player.UpdatePiecePosition(sideValue, g.board)
		fmt.Println(message)
		hasWinner = checkWinner(player, g.board)
	}
	fmt.Printf("%s wins the game\n", player.Name())
}

func checkWinner(player player.Player, board board.Board) bool {
	return board.IsPlayerAtTarget(player.CurrentPiecePosition())
}

func choosePlayer(players []player.Player, turn *int) player.Player {
	if *turn == 1 {
		*turn = 0
		return players[1]
	}
	*turn = *turn + 1
	return players[0]
}
