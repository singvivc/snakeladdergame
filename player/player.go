package player

import (
	"SnakeLadderGame/board"
	"SnakeLadderGame/dice"
	"fmt"
)

type player struct {
	piece int
	name  string
}

// Player interface that have the collection of methods to be performed by the player struct
type Player interface {
	Roll(dice.Dice) int
	Name() string
	CurrentPiecePosition() int
	UpdatePiecePosition(int, board.Board) string
}

// NewPlayer Creates a new player struct
func NewPlayer(name string) Player {
	return &player{piece: -1, name: name}
}

func (p *player) Roll(dice dice.Dice) int {
	return dice.Roll()
}

func (p *player) Name() string {
	return p.name
}

func (p *player) CurrentPiecePosition() int {
	return p.piece
}

func (p *player) UpdatePiecePosition(by int, board board.Board) string {
	position := board.NewPosition(by + p.piece)
	initialPosition := p.piece
	if position != -1 {
		p.piece = position
	}
	message := fmt.Sprintf("%s rolled a %d and moved from %d to %d", p.name, by, initialPosition, p.piece)
	return message
}
