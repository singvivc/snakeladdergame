package player

import (
	"SnakeLadderGame/board"
	"SnakeLadderGame/dice"
	"testing"

	"github.com/stretchr/testify/assert"
)

type playerStub struct {
	name          string
	piecePosition int
}

var tests = []struct {
	description string
	given       playerStub
}{
	{
		"Player A the initial piece position should be -1",
		playerStub{"Player A", -1},
	},
}

func TestShouldInitializePlayer(t *testing.T) {
	for _, test := range tests {
		player := NewPlayer(test.given.name)
		assert.Equal(t, player.Name(), test.given.name)
		assert.Equal(t, player.CurrentPiecePosition(), test.given.piecePosition)
	}
}

func TestShouldReturnRandomNumberInAGivenRange(t *testing.T) {
	dice := dice.NewDice(1)
	for _, test := range tests {
		player := NewPlayer(test.given.name)
		assert.Equal(t, player.Name(), test.given.name)
		sideValue := player.Roll(dice)
		assert.LessOrEqual(t, sideValue, 6)
		assert.GreaterOrEqual(t, sideValue, 1)
	}
}

func TestShouldUpdatePlayersPiecePositionOnBoard(t *testing.T) {
	board := board.NewBoard(100, [][]int{{45, 23}, {50, 16}}, [][]int{{34, 64}, {20, 60}})
	dice := dice.NewDice(1)
	for _, test := range tests {
		player := NewPlayer(test.given.name)
		assert.Equal(t, player.CurrentPiecePosition(), test.given.piecePosition)
		sideValue := player.Roll(dice)
		player.UpdatePiecePosition(sideValue, board)
		assert.NotEqual(t, player.CurrentPiecePosition(), test.given.piecePosition)
	}
}
